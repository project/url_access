== URL Access ==
This module adds an option to node edit forms to mark a page as 'protected'.
When submitted, this generates a UUID and adds a path to 'protected/{UUID}' to
the {url_alias} table. When accessing the node, only users with the 'Administer
nodes' permission, the node author, and those with the correct URL
can access the page.

== Installation ==
Extract the module to your modules folder and enable it on the modules page. A
{url_access} table will be created for MySQL. If you are using PostgresSQL,
please check to see if the CREATE TABLE syntax works and if so let me know
via the issue queue for this module.

== Using URL Access ==
When creating a node, check off "Require unique URL for access" under
"URL Access Restrictions". The generated link will be displayed after
submission. As well, viewing the URL Access Restrictions frameset on a
restricted node will show the protected URL. If you disable and renable URL
access protection on a node, note that a new UUID will be generated.

Note that this module does not use the normal node_access API, as it is not
able to be expressed in terms of users. This means that the node may still be
viewable by other modules, including the search mechanism. It is recommended to
pair this module with the search_block moudule to exclude protected nodes from
search.

== About ==
This module was created by Andrew Berry for Pinchin Environmental.
mailto:andrewberry@sentex.net
http://www.pinchin.com/
Project Page: http://drupal.org/project/url_access